#pragma once
#include <exception>
#include <string>
#include <map>

#include "SDL2/SDL_ttf.h" 
#include "SDL2/SDL.h"



class FontContainer
{
public:
  FontContainer()
  {}

  ~FontContainer()
  {
    clean();
  }

TTF_Font * get_font(int size)
{
  auto font_iter = m_font_container.find(size);
  if( font_iter == m_font_container.end() )
  {
    load_font(size);
    font_iter = m_font_container.find(size);
    if(font_iter == m_font_container.end())
    {
      throw std::invalid_argument("Not possible to initialize fonts.");
    }  
  }
  return font_iter->second;
}

void clean() noexcept
{
  for (auto & font : m_font_container)
  {
    TTF_CloseFont(font.second);
    font.second = nullptr;
  }
  
  m_font_container.clear();
}

private:

  void load_font(int size)
  {
    TTF_Font * tmp_font;
    tmp_font = TTF_OpenFont("src/graphics/FreeMonoBold.ttf", size);
    if (tmp_font == NULL)
    {
    throw std::invalid_argument("Could not load fonts.");
    }
    m_font_container[size] =  tmp_font;
  }
  
  std::map<int, TTF_Font *> m_font_container;

};