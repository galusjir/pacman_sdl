#ifndef CONFIG_H
#define CONFIG_H

namespace conf
{


/**              CONTROL          **/
    const int LAST_PRESSED_KEY_DEBOUNCING = 500;
    const int SPEED               = 30;


/**              GRAPHICS          **/
    const int X_SCREEN_OFFSET_PPX = 0;
    const int Y_SCREEN_OFFSET_PPX = 100;
    const int INIT_SCREEN_WIDTH = 1000;
    const int INIT_SCREEN_HEIGHT = 1000;
    const int TILE_N_PPX = 32;                // Do not change, otherwise it will break stuff
    const int SPRITESHEET_HEIGHT_PPX = 64;
    const int SPRITESHEET_WIDTH_PPX  = 256;
    const int SCALING                = 1;     // Scaling for low dp monitors
    const int FONT_SIZE              = 20;
    const int END_MESSAGE_FONT_SIZE  = 70;
    const int SCORE_FONT_SIZE        = 40;
    const double DYNAMIC_SCALING     = 0.80;


/**              TIMING          **/
    const int SCATTER_TIMER_MS = 10000;
    const int DEAD_TIMER_MS = 2000;
    const int HOLD_TIMER_MS = 1000;
    const int FPS           = 120;
    const int DIRECTION_DEBOUNCING = 500;

    const int PACMAN_MOUTH_DEBOUNCING = 5;

    const int BLINKY_SPEED_PROC = 80; /**< Percentage of speed*/
    const int INKY_SPEED_PROC = 80;
    const int CLYDE_SPEED_PROC = 80;
    const int PINKY_SPEED_PROC = 80;
    const int NOMINAL_SPEED    = 50;  /**< Corresponds to 100% speed */

/**              GRAPHICS          **/
    const int SCORE_FOR_GHOST = 500;  /**< Score for killing a ghost */
    const int MAX_MAZE_SIZE   = 50;

/**              DERIVED TYPES          **/
    const int ITER_TIME     = 1000 / FPS; 
}



#endif