#include "maze.h"
#include <limits>


Maze::Maze(std::string layout, PacManAssets & assets, PacmanEngine * engine)
{
  // Initialize the engine
  m_engine = engine;
  m_score = 0;
  m_assets = &assets;
  m_n_points = 0;
  m_init_pacman_pos_x = 0;
  m_init_pacman_pos_y = 0;

  std::ifstream in_file;
  try
  {
    load_layout(layout, in_file, assets);
  }
  catch (std::invalid_argument & e)
  {
    if(in_file.is_open())
    {
      in_file.close();
      std::cerr << e.what();
    }
    throw e; // No level, terminate the program
  }
}

Maze::~Maze()
{
  clean();
}

void Maze::clean() noexcept
{
  if(tiles_arr)
  {
    delete [] tiles_arr;
    tiles_arr = nullptr;
  }
}


void Maze::load_layout(std::string layout_path, std::ifstream & in_file, PacManAssets & assets)
{
  in_file.open(layout_path, std::ios::in); // std::ios::ate
  if(!in_file.is_open() || !in_file.good())
      throw std::invalid_argument("Level layout not found");
  
  // Read the size
  char delimiter;
  
  in_file >> m_height;
  in_file >> delimiter;
  in_file >> m_width;
  if(m_width < 0 || m_width > conf::MAX_MAZE_SIZE ||
     m_height < 0 || m_height > conf::MAX_MAZE_SIZE ||
     delimiter != 'x')
  {
    throw std::invalid_argument("Could not load the maze.");
  }
  
  std::string line;
  std::getline(in_file, line); // Ignore everything till \n
  tiles_arr = new std::shared_ptr<Tile> [m_height * m_width];
  int x = 0, y = 0;

  auto load_tile = [&] (int x, int y, std::shared_ptr<Tile> tile)
  {
    int i = x + y*m_width;
    if (i >= m_width * m_height) throw std::invalid_argument("Level layout error");
    tiles_arr[x + y*m_width] = tile;
  };

  while(std::getline(in_file, line) ) // y < m_height
  {
    x = 0;
    for (auto c = line.begin(); c != line.end(); c++, x++)
    {
      switch (*c)
      {
      case '#':  // Wall placeholder. Correct angles set later
        load_tile(x, y, std::make_shared<Tile>() );
        break;
      
      case '*': // Coin
        load_tile(x, y, std::make_shared<Path>( assets.get_texture("empty"), assets.get_texture("coin"), false ) );
        m_n_points++;
        break;

      case 'B': //Bonus
        load_tile(x, y, std::make_shared<Path>( assets.get_texture("empty"), assets.get_texture("bonus"), true ));
        m_n_points++;
        break;

      case '_': //Empty
        load_tile(x, y, std::make_shared<Path>( assets.get_texture("empty"), nullptr, false ) );
        break;
      
      case 'P': // Pacman position
        load_tile(x, y, std::make_shared<Path>( assets.get_texture("empty"), nullptr, false ) );
        m_init_pacman_pos_x = x;
        m_init_pacman_pos_y = y;
        break;

      default:
          throw std::invalid_argument("Level layout error");
        break;
      }
    }
    if (x != m_width) 
    {
        std::cout<< "Current width: " << x << "Shall be width: " << m_width << std::endl;
        std::cout << "Height: " << y << std::endl;
        throw std::invalid_argument("Level layout error, incorrect width");

    }
    y++;
  }
  if (y != m_height) throw std::invalid_argument("Level layout error, incorrect height");
  in_file.close();
  build_walls();
}


void Maze::build_walls()
{
  std::set<Position> visited_cells;
  // Go over all cells
  for (int y = 0; y < m_height; y++)
  {
    for (int x = 0; x < m_width; x++)
    {
      if (   get(x,y)->is_solid() && 
           ( visited_cells.find(Position(x,y)) == visited_cells.end())
         )
      {
        follow_wall(Position (x,y), visited_cells);
      }
    }
    
  }

}


/**
 * 
 *  There are two types of wall. Inner wall and outer walls. They differ where to look
 * 
 *  The maze is outer loop
 * 
 *  All the other walls are inner loops -> How to distinguish the types between each other???
 */

void Maze::follow_wall(Position pos, std::set<Position> & visited_cells)
{

/*******             LAMBDAS DEFINITION                      *******/
// Probably bad idea to have such a long and many lambdas, but at least it won't clatter
// the Maze namespace.
  auto new_wall = [&](Position new_pos, std::string texture, float angle)
  {
    replace(new_pos, std::make_shared<Wall>(PacManAssets::get().get_texture(texture), angle, SDL_FLIP_NONE));
  };

  auto check_turn = [&] (Position pos, Direction dir, Direction (*check_side_func)(Direction)) -> bool
  {
    CrossArrow arrow = get_cross_arrow(pos);
    if (get(pos)->is_solid()  &&  arrow( check_side_func(dir) ) == TileType::PATH)
    {
      return true;
    }
    return false;
  };

  auto calc_corner_angle = [] (bool prev_turn_was_right, bool right_turn, int angle) -> int
  {
    if ( prev_turn_was_right ^ right_turn)
    {
      angle += 180;
    }
    else if (right_turn)
    {
      angle += 90;
    }
    else
    {
      angle -= 90;
    }
    return angle < 360 ? angle : angle % 360;
  };
/*********************************************************************/


  Direction dir = NONE;
  Direction (*side_select_func)(Direction dir) = nullptr;
  int wall_angle = 0;   // Will undergo int -> float promotion! 
  int corner_angle = 0;
  int prev_turn_was_right;

//**  Initialization of the movement!! **//
  CrossArrow arrow = get_cross_arrow(pos);
  if (arrow.SOUTH  == TileType::SOLID && arrow.WEST == TileType::SOLID)
  {
    dir = NORTH;
    prev_turn_was_right = true;
    corner_angle = -90;
    if (arrow.NORTH == TileType::PATH)
    {
       side_select_func = turn_left;
    }
    else
    {
      side_select_func = turn_right;
    }
  }
  // Cover the use case, when there is a broken link and algo must start downwards
  else if( arrow.SOUTH  == TileType::SOLID)
  {
      dir = SOUTH;
      prev_turn_was_right = false;
      if (arrow.WEST == TileType::PATH)
      {
          side_select_func = turn_left;
      }
      else if(arrow.EAST == TileType::PATH)
      {
          side_select_func = turn_right;
      }
      else
      {
          visited_cells.insert(pos);
          return;
      }
  }
  else
  {
    visited_cells.insert(pos);
    return;
  }
  
//**  Run the loop over the wall **//
  int run_the_loop = true;
  while( visited_cells.find(pos) == visited_cells.end() && run_the_loop)
  {
      arrow = get_cross_arrow(pos);
      //  TRY TO GO STRAIGHT
      if ( arrow( side_select_func(dir) ) == TileType::PATH &&
           arrow(dir)                     == TileType::SOLID  )
      {
        new_wall(pos, "wall", wall_angle);
      }

      //  TRY TO TURN RIGHT
      else if(  check_turn(pos + turn_right(dir), turn_right(dir), side_select_func) ||
                (check_turn(pos, turn_right(dir), side_select_func) && arrow(turn_right(dir)) == TileType::SOLID )
             )
            
      {
        wall_angle = (wall_angle + 90) % 360;
        corner_angle = calc_corner_angle(prev_turn_was_right, true, corner_angle);
        new_wall(pos, "corner", corner_angle);
        dir = turn_right(dir);
        prev_turn_was_right = true;
        
      }

      // TRY TO TURN LEFT
      else if (  check_turn(pos + turn_left(dir), turn_left(dir), side_select_func)  )
      { // turn to left
        wall_angle = (wall_angle - 90) % 360;
        corner_angle = calc_corner_angle(prev_turn_was_right, false, corner_angle);
        
        new_wall(pos, "corner", corner_angle);
        dir = turn_left(dir);
        prev_turn_was_right = false;
      }


      // Not possible to go further -- Possible reached the teleport
      else
      {
        run_the_loop = false;
        new_wall(pos, "wall", wall_angle);
        std::cout << "Breaking the loop" << std::endl;
      }

      visited_cells.insert(pos);
      pos = pos + dir;
     
  }


}


bool Maze::replace(int x, int y, std::shared_ptr<Tile> tile)
{
  tiles_arr[x + y*m_width] = tile;
  return true;
}

bool Maze::replace(Position pos, std::shared_ptr<Tile> tile)
{
  tiles_arr[pos.get_x() + pos.get_y()*m_width] = tile;
  return true;
}


void Maze::draw(PacmanEngine * engine)
{
    for (int y = 0; y < m_height; y++)
    {
       for (int x = 0; x < m_width; x++)
       {
         tiles_arr[x + y * m_width]->draw(engine, x, y);
       }

    }
}

bool Maze::step_on(int x, int y) const
{
  bool ret_val = false;
  if (x >= 0 && x < m_width &&
      y >= 0 && y < m_height)
  {
    if(!get(x,y)->is_solid())
    {
      ret_val = true;
    }
  }
  return ret_val;
}

int Maze::step_on_bonus(int x, int y)
{
  int bonus = -1;
  if (x >= 0 && x < m_width &&
      y >= 0 && y < m_height)
  {
    bonus = get(x,y)->step_in();
    {
      if (bonus > 0)
      {
        m_n_points--;
        m_score += bonus;
      }
      if(bonus == BONUS_VALUE)
      {
        m_engine->set_scatter_mode(conf::SCATTER_TIMER_MS); // TODO Implement table of scatter mode durations
      }
    }
  }
  return bonus;
}

CrossArrow Maze::get_cross_arrow(Position pos) const
{
  return get_cross_arrow(pos.get_x(), pos.get_y());
}

CrossArrow Maze::get_cross_arrow(int x_coor, int y_coor) const
{
  CrossArrow dir;
      // Check NORTH
  if (y_coor > 0)
  {
      if(get(x_coor, y_coor - 1)->is_solid())
          dir.NORTH = TileType::SOLID;
  }
  else
  {
      dir.NORTH = TileType::BORDER;
  }
    
  // Check SOUTH
  if (y_coor < (m_height - 1) )
  {
      if(get(x_coor, y_coor + 1)->is_solid())
          dir.SOUTH = TileType::SOLID;
  }
  else
  {
      dir.SOUTH = TileType::BORDER;
  }

  // Check EAST
  if (x_coor > 0)
  {
      if(get(x_coor - 1, y_coor)->is_solid())
        dir.EAST = TileType::SOLID;
  }
  else
  { 
      dir.EAST = TileType::BORDER;
  }

  // Check WEST
  if (x_coor < (m_width - 1))
  {
      if(get(x_coor + 1, y_coor)->is_solid())
          dir.WEST = TileType::SOLID;
  }
  else
  {
      dir.WEST = TileType::BORDER;
  }  
  return dir;
}


Direction Maze::path_find(Position source, Position target, Direction curr_dir) const
{
  CrossArrow cross = get_cross_arrow(source);
  Direction next_dir = NONE;
  double min_dist = std::numeric_limits<double>::infinity();
  
  auto check_shortest_path = [&] (const Direction dir)
  {
    Position tmp = Position(source) + dir;
    double d = tmp.get_distance(target);
    if (d < min_dist)
    {
      min_dist = d;
      next_dir = dir;
    }
  };

  // Precedence is [up, left, down, right]
  // Ghost cannot switch to oposite direction
  if( cross.NORTH == TileType::PATH && curr_dir != SOUTH)
  {
    check_shortest_path(NORTH);
  }
  if (cross.EAST == TileType::PATH && curr_dir != WEST)
  {
    check_shortest_path(EAST);
  }
  if (cross.SOUTH == TileType::PATH && curr_dir != NORTH)
  {
    check_shortest_path(SOUTH);
  }
  if (cross.WEST == TileType::PATH && curr_dir != EAST)
  {
    check_shortest_path(WEST);
  }
  
  return next_dir;
}

Position Maze::get_penhouse_pos(Direction dir)
{
  // Assume, the penhouse is in the middle
  int pos_y = m_height / 2;
  int pos_x = m_width / 2;
  CrossArrow cross = get_cross_arrow(pos_x, pos_y);
  if (step_on(pos_x, pos_y) == 0)
  { // Standing on a wall
    if (cross.NORTH != TileType::SOLID)
    {
      pos_y--;
    }
    else if (cross.SOUTH != TileType::SOLID)
    {
      pos_y++;
    }
  }
  cross = get_cross_arrow(pos_x, pos_y);
  if(dir == NORTH)
  {
    while (cross.NORTH != TileType::SOLID)
    {
      cross = get_cross_arrow(pos_x, --pos_y);
    }
  }
  else if(dir == WEST)
  {
    while (cross.WEST != TileType::SOLID)
    {
      cross = get_cross_arrow(++pos_x, pos_y);
    }
  }
  else if (dir == EAST)
  {
    while (cross.EAST != TileType::SOLID)
    {
      cross = get_cross_arrow(--pos_x, pos_y);
    }
  }
  else if (dir == SOUTH || dir == NONE)
  {

  }
  else
  {
    throw std::invalid_argument("Wrong position in penhous");
  }
  
  return Position(pos_x, pos_y);

}