#pragma once
#include <cmath>
#include "pacmanEngine.h"
#include "maze.h"
#include "direction.h"
#include "config.h"

class PacmanEngine;
class Maze;

/** @brief Abstract class of a dynamic object, which interacts upon the Maze
 */
class DynamicObject
{
public:
  virtual ~DynamicObject() {}

  /** @brief 
   * 
   *  @param pos_x initial position in x
   *  @param pos_y initial position in y
   *  @param sprt  SDL_Texture rendered by GameEngine which is used in draw function
   */
  DynamicObject(int pos_x, int pos_y, SDL_Texture * sptr);
  
  /** @brief Implement custom logic for key_press from the keyboard
   * 
   *  @param last_key Input from the keyboard
   *  @param maze  Maze for interaction with surrounding worls
   */
  virtual int next_step(GameEngine::KEY_PRESS last_key, Maze * maze) = 0;
  
  /** @brief Draws the object
   * 
   *  @param engine must be the same instance of the engine which was used for
   *                rendering the spritesheets
   */
  virtual void draw(GameEngine * engine) const;

  /** @brief Resets the object to the initial position
   *  
   *  @param x,y reset to position x, y
   */
  virtual void reset(int x, int y);
  virtual void reset();

  /** @brief Sets the % of the pacman speed
   *  @param speed_proc in %. 100 corresponds to pacman speed  
   */
  virtual void set_speed(int speed_proc);

  /**
   *  @brief returns x, y position of the object on the maze
   */
  std::pair<int, int> get_position() const;

  /** @brief return current traveling direction
   * 
   */
  virtual GameEngine::KEY_PRESS get_direction() const {return m_direction;}

   /** @brief Turne the ghost mode into scater mode with targets in the maze corners
   * 
   */
  virtual void scatter_mode() = 0;
  
  /** @brief Change the ghost mode into chase mode - chasing the pacman
   * 
   */
  virtual void chase_mode() = 0;

  /** @brief Kills the dynamic object if it's possible
   *  
   *  Compares if the object is mortal and is not in dead, or hold state
   */
  virtual bool kill();



//*****************    IMPLEMENTATION DETAILS   **************************************//
protected:
  /** @brief Implements the change of position based on the key press
   * 
   *  Step function is common for both dynamic objects.
   */
  int step(GameEngine::KEY_PRESS direction, Maze * maze);

  
  
  virtual void set_alive_tile() {m_display = 0;}
  virtual void set_hold_tile() {m_display = 2;}   /**< Hold tile is for ghost scatter*/
  virtual void set_dead_tile() {m_display = 3; m_angle = 0; m_flip = SDL_FLIP_NONE;}

  virtual void set_hold_timer() {m_hold_timer = conf::HOLD_TIMER_MS / conf::ITER_TIME;}
  virtual void set_dead_timer() {m_kill_timer = conf::DEAD_TIMER_MS / conf::ITER_TIME;}
  
  /** @brief Check if pacman is in process of death, or is in hold
   * 
   *  @return true if dynamic object is dead, or in hold state -> should not move!
   */
  virtual bool am_i_dead();

  /**
   * @brief Increments given coordinate. Can work with arbitrary m_speed
   */
  int pos_increment(int pos, bool negative) const;
  

//****************   PROTECTED ATRIBUTES     ***************************************//

  int pos_x_1000th;          /**< X position of the object in maze, in 100th */
  int pos_y_1000th;          /**< Y position of the object in maze, in 100th */
  int m_speed = conf::SPEED;       
  
  GameEngine::KEY_PRESS m_direction = GameEngine::KEY_PRESS_DEFAULT;

  std::pair<int, int> current_tile;  /**< Index of last visited / current tile -> Data duplication to pos_x, pos_y*/
  
  int init_pos_x_1000th;    /**< Initial x position for a restart */
  int init_pos_y_1000th;    /**< Initial y position for a restart */

// ** DRAW atribues 
  SDL_Texture * m_sprt;     /**< 256x64 pixeles sheet */
  int m_display = 0;      /**< Which sprite shall be displayed */
  
  float m_angle = 0.0F;   /**< Angle of the sprite */
  SDL_RendererFlip m_flip = SDL_FLIP_NONE;
  
// ** KILL SEQUENCE **/
  int m_kill_timer;       /**< when 0, normal operation */
  int m_hold_timer;
  bool m_immortal;
};








/***************************************************/
/**           CLASS PacmanPlayer                  **/
/***************************************************/

class PacmanPlayer : public DynamicObject 
{
public:
  virtual ~PacmanPlayer() {}
  PacmanPlayer(int x, int y, SDL_Texture * sprt);

  /**
   * @brief Custom stepping function for pacman
   * 
   *  Then calls DynamicObject::Step();
   */
  virtual int next_step(GameEngine::KEY_PRESS last_key, Maze * maze) override;

   /** @brief Turne the ghost mode into scater mode with targets in the maze corners
   * 
   */
  virtual void scatter_mode() override;
  
  /** @brief Change the ghost mode into chase mode - chasing the pacman
   * 
   */
  virtual void chase_mode() override;

  /**
   * @brief Needs to initialize additional attributes
   */
  virtual void reset() override;

protected:

  enum SPRITE_DISPLAY
  {
    OPEN   = 0,
    CLOSED = 1,
    FULL   = 2,
    DEAD   = 3
  };

  GameEngine::KEY_PRESS m_last_pressed_key = GameEngine::KEY_PRESS_DEFAULT;

  const int mouth_debouncing = (conf::PACMAN_MOUTH_DEBOUNCING * conf::FPS) / conf::SPEED;
  int m_last_pressed_key_debouncing = conf::LAST_PRESSED_KEY_DEBOUNCING;
};





/***************************************************/
/**               CLASS GHOST                     **/
/***************************************************/


class Ghost : public DynamicObject
{
public:
  Ghost(int pos_x, int pos_y, SDL_Texture * sprt, PacmanEngine * engine);
  virtual ~Ghost() noexcept {}
  
  /**
   * @brief Custom stepping function for pacman
   * 
   *  Then calls DynamicObject::Step();
   */
  virtual int next_step (GameEngine::KEY_PRESS last_key, Maze * maze);

  /** @brief Turne the ghost mode into scater mode with targets in the maze corners
   * 
   */
  virtual void scatter_mode() override;
  
  /** @brief Change the ghost mode into chase mode - chasing the pacman
   * 
   */
  virtual void chase_mode() override;
  
  virtual void reset() override;

  virtual GameEngine::KEY_PRESS get_direction() const {return dir_to_key(m_direction);}

protected:
  
  /** @brief Gets a target for a new move. Each ghost must override
   * 
   */
  virtual Position get_target() const = 0;
  
  /** @brief Gets a scated targe. Each ghost must override this function
   * 
   *  In scater mode, the target position shall be in corners
   */
  virtual Position get_scater_target() const = 0;
  
  virtual void set_hold_tile() override 
  {
    if (!m_kill_timer && !m_hold_timer)
      m_display = 0;
  }
  virtual void set_scater_tile() {m_display = 2;}

  
  

  enum SPRITE_DISPLAY
  {
    NORMAL   = 0,
    RESERVED = 1,
    SCARED   = 2,
    DEAD     = 3
  };
  
  PacmanEngine * m_p_engine;     /**< Pacman engine to interact with other dynamic objects */
  Direction m_direction;         /**< Covers the original move direction -> not nice, but it ease planning */
  Direction m_next_direction;    /**< Planning of a next direction */

};





/***************************************************/
/**      CLASS INKY, PINKY, BLINKY, AND CLYDE     **/
/***************************************************/

class Blinky : public Ghost
{
public:
  Blinky(int pos_x, int pos_y, SDL_Texture * sprt, PacmanEngine * engine):
  Ghost(pos_x, pos_y, sprt, engine)
  { set_hold_timer();}

  virtual ~Blinky() noexcept{}

protected:
  virtual Position get_target() const override;
  virtual Position get_scater_target() const override;

  /** @brief Blinky beeing egressive has 0 timer
   * 
   */
  virtual void set_hold_timer() override {m_hold_timer = 50;}
};



class Pinky : public Ghost
{
public:
  Pinky(int pos_x, int pos_y, SDL_Texture * sprt, PacmanEngine * engine):
  Ghost(pos_x, pos_y, sprt, engine)
  {set_hold_timer();}

  virtual ~Pinky() noexcept {}

protected:
  virtual Position get_target() const override;
  virtual Position get_scater_target() const override;
  
  /** @brief Blinky beeing egressive has 0 timer
   * 
   */
  virtual void set_hold_timer() override {m_hold_timer = 200;}
};

class Inky : public Ghost
{
public:
  Inky(int pos_x, int pos_y, SDL_Texture * sprt, PacmanEngine * engine):
  Ghost(pos_x, pos_y, sprt, engine)
  {set_hold_timer();}

  virtual ~Inky() noexcept {}


protected:
  virtual Position get_target() const override;
  virtual Position get_scater_target() const override;

  /** @brief Blinky beeing egressive has 0 timer
   * 
   */
  virtual void set_hold_timer() override {m_hold_timer = 100;}
};



class Clyde : public Ghost
{
public:
  Clyde(int pos_x, int pos_y, SDL_Texture * sprt, PacmanEngine * engine):
  Ghost(pos_x, pos_y, sprt, engine)
  {set_hold_timer();}

  virtual ~Clyde() noexcept{}

protected:
  virtual Position get_target() const override;
  virtual Position get_scater_target() const override;

  /** @brief Blinky beeing egressive has 0 timer
   * 
   */
  virtual void set_hold_timer() override {m_hold_timer = 300;}
};