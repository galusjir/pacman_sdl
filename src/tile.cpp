#include "tile.h"


//0***************************************************0
//                   Wall
//0***************************************************0

Wall::Wall(SDL_Texture * texture, float angle, SDL_RendererFlip flip)
{
  m_solid = true;
  m_texture = texture;
  m_angle = angle;
  m_flip = flip;
}

void Wall::draw(GameEngine * engine, int x, int y) const
{
  engine->draw_sprite(m_texture, x , y , m_angle, m_flip);
}

//0***************************************************0
//                   Path
//0***************************************************0

Path::Path(SDL_Texture * t0, SDL_Texture * t1, bool is_bonus)
{
  m_solid = false;
  textures[0] = t0;
  textures[1] = t1;
  if(t1)
  {
    m_draw_texture = 1;
    m_is_bonus = is_bonus;
    if (is_bonus)
      m_score = BONUS_VALUE;
    else
      m_score = COIN_VALUE;
  }
  else
  {
    m_draw_texture = 0;
    m_is_bonus = 0;
    m_score = 0;
  }
}

int Path::step_in()
{
  m_draw_texture = 0;
  m_is_bonus = false;
  int ret_score = m_score;
  m_score = 0;
  return ret_score;
}

void Path::draw(GameEngine * engine, int x, int y) const
{
  engine->draw_sprite(textures[m_draw_texture], x,y, 0, SDL_FLIP_NONE);
}