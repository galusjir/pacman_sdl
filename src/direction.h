#pragma once
#include <cstdint>
#include "gameEngine.h"

enum Direction
{
    NONE  = 0,
    NORTH = 1,
    WEST  = 2,
    SOUTH = 3,
    EAST  = 4 
};

// Possible implementation of type safer enum class with
// ability to bitmasks
// https://stackoverflow.com/questions/12059774/c11-standard-conformant-bitmasks-using-enum-class

enum class TileType
{
  PATH,
  SOLID,
  BORDER
};

/** @brief Represent tile types around current tile
 * 
 */
class CrossArrow
{
public:
  TileType NORTH = TileType::PATH;
  TileType SOUTH = TileType::PATH;
  TileType WEST = TileType::PATH;
  TileType EAST = TileType::PATH;
  
  TileType operator() (Direction dir)
  {
    TileType tp;
    switch (dir)
    {
      case Direction::NORTH: tp = this->NORTH; break;
      case Direction::WEST: tp = this->WEST;   break;
      case Direction::SOUTH: tp = this->SOUTH; break;
      case Direction::EAST: tp = this->EAST;   break;
      default: throw std::invalid_argument("Invalid indexing in CrossArrow");
    }
    return tp;
  }
};

/** @brief Translate direction to key press
 *  
 *  It's a result of a inconsistent direction formats and result
 *  of a bad design
 */
GameEngine::KEY_PRESS dir_to_key(Direction dir);
Direction key_to_dir(GameEngine::KEY_PRESS key);

// Change of direction
Direction turn_right(Direction dir);
Direction turn_left (Direction dir);
Direction reverse_dir(Direction dir);


/** @brief wraps position in a simple class
 * 
 *  Has  feature of summing direction with possition to get new position
 */
class Position
{
public:
  Position():
  m_x(0), m_y(0)
  {}

  Position(std::pair<int, int> dir) :
  m_x(dir.first), m_y(dir.second)
  {}

  Position(int x, int y)
  {
    m_x = x;
    m_y = y;
  }
  
  bool operator< (const Position rhs) const
  {
    bool ret_val;
    if ( m_y < rhs.m_y)
      ret_val = true;
    else
      if (m_y == rhs.m_y && m_x < rhs.m_x)
        ret_val = true;
      else
        ret_val = false;
    return ret_val;
  }

  std::pair<int, int> get_pair() const
  {
    return std::make_pair(m_x, m_y);
  }

  int get_x () {return m_x;}
  int get_y () {return m_y;}

  Position operator + (const GameEngine::KEY_PRESS dir) const
  {
    return *this + key_to_dir(dir);
  }

  Position operator + (const Direction dir) const
  {
    Position pos = {m_x, m_y};
    switch(dir)
    {
      case NORTH:
        if (m_y > 0) pos.m_y--;
        break;

      case SOUTH:
        pos.m_y++;
        break;
      
      case WEST:
        pos.m_x++;
        break;

      case EAST:
        if (m_x > 0) pos.m_x--;
        break;
      
      default:
        break;
    }
    return pos;
  }

  bool operator == (const Position rhs)
  {
    if ( (this->m_x == rhs.m_x) && 
         (this->m_y == rhs.m_y)
       )
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  double get_distance(const Position pos)
  {
    return sqrt( pow((pos.m_x - this->m_x), 2) +
                 pow((pos.m_y - this->m_y), 2)
               );
  }
private:
 int m_x;
 int m_y;

};





//         OBSOLETE

//return this->operator+(key_to_dir(dir));
    /*
    Position pos = {m_x, m_y};
    switch(dir)
    {
      case GameEngine::KEY_PRESS_UP:
        if (m_y > 0) m_y--;
        break;

      case GameEngine::KEY_PRESS_DOWN:
        m_y++;
        break;
      
      case GameEngine::KEY_PRESS_RIGHT:
        m_x++;
        break;

      case GameEngine::KEY_PRESS_LEFT:
        if (m_x > 0) m_x--;
        break;
    }
    return pos;
    */