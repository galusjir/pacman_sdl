#include "gameEngine.h"

GameEngine::GameEngine()
{
  m_app_name = "VOID";
  m_game_active = false;
  m_gWindow = NULL;
  m_gRenderer = NULL;
  m_x_screen_offset = conf::X_SCREEN_OFFSET_PPX;
  m_y_screen_offset = conf::Y_SCREEN_OFFSET_PPX;
  pressed_key = KEY_PRESS_DEFAULT;
}

GameEngine::~GameEngine()
{
  
}

void GameEngine::quit()
{

  clear_screen();
  SDL_DestroyRenderer(m_gRenderer);
  SDL_DestroyWindow(m_gWindow);
  TTF_Quit();
  SDL_Quit();
  IMG_Quit();
}

void GameEngine::start()
{
  m_game_active = true;
                    // thread runs game_thread() on this    
  std::thread th = std::thread(&GameEngine::game_thread, this);
  th.join();
}

bool GameEngine::init_window()
{
  if(SDL_Init(SDL_INIT_VIDEO) < 0 || TTF_Init() < 0) // TODO: Add error handling, leave it now to be clean
  {
    throw std::runtime_error("Not possible to initialize SDL");
  }
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  // Create the window
  m_gWindow = SDL_CreateWindow(m_app_name.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                conf::INIT_SCREEN_WIDTH, conf::INIT_SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

  IMG_Init(IMG_INIT_PNG);  
  
  // Create the Renderer
  m_gRenderer = SDL_CreateRenderer( m_gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);
  
  if (m_gWindow == NULL || m_gRenderer == NULL )
  {
    throw std::runtime_error("Window initialization failed");
  }
  SDL_SetRenderDrawColor (m_gRenderer, 0xFF,0xFF,0xFF,0xFF);
  SDL_RenderClear(m_gRenderer);
  SDL_RenderPresent(m_gRenderer);

  return true;
}

/** @brief The engine shall handle the scaling and drawing to the screen.
 *         It can utilize offset to the screen
 * 
 *         TODO Implement better scaling 
 */
int GameEngine::draw_sprite(SDL_Texture * texture, int pos_x, int pos_y, double angle,
                            SDL_RendererFlip flip, int offset_x, int offset_y)
{
  int x = (pos_x * 32 + m_x_screen_offset + offset_x) * conf::SCALING;
  int y = (pos_y * 32 + m_y_screen_offset + offset_y) * conf::SCALING;
  SDL_Rect rect = {x, y, 32 * conf::SCALING, 32 * conf::SCALING};
  SDL_RenderCopyEx(m_gRenderer, texture, NULL, &rect, angle, NULL, flip);
  
  return 0;
}

int GameEngine::draw_spritesheet(SDL_Texture * texture, float pos_x, float pos_y, double angle,
                                 SDL_RendererFlip flip, SDL_Rect clipRect, int offset)
{
  int h = static_cast<int>( 64 * conf::SCALING * 0.80);
  int w = static_cast<int>( 64 * conf::SCALING * 0.80);
  int centering = (64 * conf::SCALING - h) / 2 ;
  int x = static_cast<int>( (pos_x * 32.0F + m_x_screen_offset + offset) * conf::SCALING ) + centering;
  int y = static_cast<int>( (pos_y * 32.0F + m_y_screen_offset + offset) * conf::SCALING ) + centering;
  
  
  SDL_Rect rect = {x, y, w, h};
  SDL_RenderCopyEx(m_gRenderer, texture, &clipRect, &rect, angle, NULL, flip);
  
  return 0;
}

void GameEngine::resize_window(int screen_width, int screen_height)
{
  SDL_SetWindowSize(m_gWindow, screen_width, screen_height);
}

void GameEngine::draw_text_in_grid(const std::string & message, float pos_x, float pos_y, int font_size)
{
  draw_text(message, pos_x * conf::TILE_N_PPX, pos_y * conf::TILE_N_PPX, 0.0F, font_size);
}

void GameEngine::draw_text(const std::string & message, float x_px, float y_px, float angle, int font_size)
{
  // Prepare the texture
  SDL_Color color = {0,0,0};
  TTF_Font * font = m_font_container.get_font(font_size);
  SDL_Surface * text_surf = TTF_RenderText_Solid(font, message.c_str(), color);
  int text_width = text_surf->w;
  int text_height = text_surf->h;
  if (text_surf == NULL)
  {
    throw std::runtime_error("Could not render text");
  }
  SDL_Texture * text_texture = SDL_CreateTextureFromSurface( m_gRenderer, text_surf);
  SDL_FreeSurface(text_surf);
  text_surf = NULL;

  int x = static_cast<int>(x_px * conf::SCALING);
  int y = static_cast<int>(y_px * conf::SCALING);

  SDL_Rect rect = {x, y, text_width * conf::SCALING, text_height * conf::SCALING};
  
  SDL_RenderCopyEx(m_gRenderer, text_texture, NULL, &rect, angle, NULL, SDL_FLIP_NONE);
  m_text_pool.push_back(text_texture);
}

void GameEngine::clear_screen()
{
  SDL_RenderClear(m_gRenderer);
  while( !m_text_pool.empty() )
  {
    SDL_DestroyTexture(m_text_pool.back());
    m_text_pool.pop_back();
  }
}

//**************** GAME THREAD ****************

void GameEngine::game_thread()
{
  init_window();
  
  if (!OnUserCreate())
  {
      m_game_active = false;
  }
  else
  {
      m_game_active = true;
  }

  {  // Resize of the window to corresponding size 
    int screen_width, screen_height;
    get_screen_dimentions(screen_width, screen_height);
    resize_window(screen_width, screen_height);
  }

  auto t1 = std::chrono::system_clock::now();
  auto t2 = std::chrono::system_clock::now();
  float elapsed_time;
  while (m_game_active)
  {
    clear_screen();
    //******* Handle relativ time between frames ******/
    t1 = std::chrono::system_clock::now();
    std::chrono::duration<float> chrono_elapsed = t1 - t2;
    t2 = t1;
    elapsed_time = chrono_elapsed.count();
    //******* Handle the user input ******/
    SDL_Event m_event;
    while(SDL_PollEvent(&m_event) != 0)
    {
        if (m_event.type == SDL_QUIT)
        {
            m_game_active = false;
        }
        else if (m_event.type == SDL_KEYDOWN)
        {
          switch (m_event.key.keysym.sym)
          {
            case SDLK_UP:
                  pressed_key = KEY_PRESS_UP;
                  break;

            case SDLK_DOWN: 
                  pressed_key = KEY_PRESS_DOWN;
                  break;

            case SDLK_LEFT: 
                  pressed_key = KEY_PRESS_LEFT;
                  break;

            case SDLK_RIGHT: 
                  pressed_key = KEY_PRESS_RIGHT;
                  break;

            default: pressed_key = KEY_PRESS_DEFAULT;
          }
        }
        else
        {
          pressed_key = KEY_PRESS_DEFAULT;
        }
    }
    
    if(!OnUserUpdate(elapsed_time))
    {
        m_game_active = false;
    }
    
    SDL_RenderPresent(m_gRenderer);

    // Mantain 60 frames per second
    int fps60_sleep = static_cast<int>(conf::ITER_TIME - elapsed_time);
    if( fps60_sleep > 0)
    {
      //std::cout << "Sleeping for: " << fps60_sleep << "Elapsed time: " << elapsed_time  << std::endl;
        std::this_thread::sleep_for 
              (std::chrono::milliseconds(fps60_sleep));
    }

  }
  // Clean the mess
  quit();

}
