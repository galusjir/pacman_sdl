
#include "pacmanEngine.h"
#include <algorithm>

PacmanEngine::~PacmanEngine()
{ }

void PacmanEngine::quit()
{
  clear_screen();
  std::cout<< "Cleaning of the content" << std::endl;
  
  maze->clean(); // Remove tiles
  if (maze) delete maze;

  PacManAssets::get().clean();  // Destroy all the textures
  m_font_container.clean();     // Destroy all fonts

  // Clean dynamic objects
  for (auto & dyn_obj : m_dyn_obj)
  {
    delete dyn_obj.second;
    dyn_obj.second = nullptr;
  }
  m_dyn_obj.clear();

  // Kill the SDL and renderer
  GameEngine::quit();
  std::cout<< "Cleaning is finished" << std::endl;
}

void PacmanEngine::get_screen_dimentions(int & screen_width, int & screen_height) const
{
  if (maze != nullptr)
  {
    int width, height;
    maze->get_n_tiles(width, height);
    if (width > 0 && height > 0)
    {
      screen_width = width * conf::TILE_N_PPX * conf::SCALING + conf::X_SCREEN_OFFSET_PPX;
      screen_height = height * conf::TILE_N_PPX * conf::SCALING + conf::Y_SCREEN_OFFSET_PPX;
    }
  }
  else
  {
    throw std::invalid_argument("Not possible to resize window, maze not constructed.");
  }
  
}

bool  PacmanEngine::OnUserCreate()
{
  PacManAssets::get().set_renderer(this->m_gRenderer);
  PacManAssets::get().load_texture();
  maze = new Maze(std::string("playground.txt"), PacManAssets::get(), this );
  initialize_dynamics(PacManAssets::get());
  n_pacman_lives = 3;
  return true;
}



bool PacmanEngine::OnUserUpdate (float elapsed_time)
{

  check_switch_to_chase_mode();
  check_dynamic_object_collisions();
  draw_stats();
  // If finished then show the score and done!!
  if(maze->finish() || n_pacman_lives == 0)
  { // SHOW SOME TEXT
    display_end_message();
    return false;  // Finish the game
  }

  // ** DRAWING **
  maze->draw(this);
  for (auto & [__key, dyn_obj] : m_dyn_obj)
  {
    UNUSED(__key);
    dyn_obj->next_step(pressed_key, maze);
    dyn_obj->draw(this);
  }

  return true;
}

void PacmanEngine::initialize_dynamics(PacManAssets & assets)
{
  int x; int y;
  maze->get_pacman_init_position(x, y);
  m_dyn_obj.insert( std::make_pair("pacman", new PacmanPlayer(x, y,assets.get_texture("pacman_sprt")) ) );
  
  Position init_pos;
  
  init_pos = maze->get_penhouse_pos(NORTH);
  // Insert the other ghosts. Not checking if insertion was successfull
  m_dyn_obj.insert(std::make_pair(std::string("blinky"), 
    new Blinky(init_pos.get_x(), init_pos.get_y(),assets.get_texture("blinky_sprt"), this) ));
  m_dyn_obj.find("blinky")->second->set_speed(conf::BLINKY_SPEED_PROC);

  init_pos = maze->get_penhouse_pos(NONE);
  m_dyn_obj.insert(std::make_pair("pinky",  
    new Pinky(init_pos.get_x(), init_pos.get_y(),assets.get_texture("pinky_sprt"), this) ));
  m_dyn_obj.find("pinky")->second->set_speed(conf::PINKY_SPEED_PROC);

  init_pos = maze->get_penhouse_pos(WEST);
  m_dyn_obj.insert(std::make_pair("clyde",  
    new Clyde(init_pos.get_x(), init_pos.get_y(), assets.get_texture("clyde_sprt"),   this) ));
  m_dyn_obj.find("clyde")->second->set_speed(conf::CLYDE_SPEED_PROC);
  
  init_pos = maze->get_penhouse_pos(EAST);
  m_dyn_obj.insert(std::make_pair("inky", 
    new Inky(init_pos.get_x(), init_pos.get_y(), assets.get_texture("inky_sprt"),   this) ));
  m_dyn_obj.find("inky")->second->set_speed(conf::INKY_SPEED_PROC);
}

std::pair<Position, GameEngine::KEY_PRESS> PacmanEngine::get_dynamic_object_position(const std::string & name) const
{
    auto dyn_obj = m_dyn_obj.find(name);
    if ( dyn_obj == m_dyn_obj.end() )
    {
      throw std::invalid_argument("Could not find asset: " + name);
    }
    
    return std::make_pair<Position, GameEngine::KEY_PRESS>
    (dyn_obj->second->get_position(), dyn_obj->second->get_direction());
  }


void PacmanEngine::set_scatter_mode(int duration_ms)
{
  m_scatter_timer = duration_ms / conf::ITER_TIME; // Game shall maintain 60fps - 
  for (auto & [__key, object] : m_dyn_obj)
  {
    UNUSED(__key);
    object->scatter_mode();
  }
}

void PacmanEngine::set_chase_mode()
{
  for(auto & [__key, object] : m_dyn_obj)
  {
    UNUSED(__key);
    object->chase_mode();
  }
}

void PacmanEngine::check_dynamic_object_collisions()
{
  DynamicObject * pacman = m_dyn_obj.find("pacman")->second;
  Position pacman_pos = pacman->get_position();
  for (auto & [key, object] : m_dyn_obj)
  {
    if (key.compare("pacman") == 0) continue;
    
    if ( pacman_pos == object->get_position())
    {
      if(pacman->kill())
      {
        n_pacman_lives--;
        break;
      }
      if(object->kill())
      {
        maze->killed_ghost();
      }
    }

  }

}

void PacmanEngine::display_end_message()
{
  clear_screen();
  std::cout << "This is an end message" << std::endl;
  int w,h;
  get_screen_dimentions(w,h);
  int text_height = h / 3;
  const int text_width = 50;
  
  draw_text(" Game over", text_width, text_height, 0.0F, conf::END_MESSAGE_FONT_SIZE);
  int score = maze->get_score();
  draw_text(" Your score is: " + std::to_string(score), text_width, text_height + conf::END_MESSAGE_FONT_SIZE * 1.2,
            0.0F, conf::END_MESSAGE_FONT_SIZE * 0.8); // Drawing in the frame
  SDL_RenderPresent(m_gRenderer);
  std::this_thread::sleep_for(std::chrono::seconds(5));
}

void PacmanEngine::draw_stats()
{
  int score = maze->get_score();
  SDL_Texture * heart_sprt = PacManAssets::get().get_texture("heart");
  draw_text_in_grid(std::string("Score: ") + std::to_string(score), 5, 1, conf::SCORE_FONT_SIZE);
  for ( int i = 1; i < (n_pacman_lives + 1); i++ )
  {
    draw_sprite(heart_sprt, i, 1, 0.0F, SDL_FLIP_NONE, 0, -76);
  }
  heart_sprt = NULL;
}

void PacmanEngine::check_switch_to_chase_mode()
{
  auto switch_to_normal = [](const std::pair<std::string, DynamicObject *> & obj_pair)
  {
    obj_pair.second->chase_mode();
  };

  if (m_scatter_timer > 0)
  {
    m_scatter_timer--;
    if (m_scatter_timer == 0)
    {
      std::for_each(m_dyn_obj.begin(), m_dyn_obj.end(), switch_to_normal);
    }
  }
  
}

int main(void)
{
    PacmanEngine pacman;
    // It's not possible to init the Renderer on different thread than
    // the gameEngine is running. The GL contex is not activated
    //pacman.init_window(); // Initialization of SDL on another thread is not possible
    
    // This will initiate the OnUserCreate and will triger the OnUserUpdate
    pacman.start();
    std::cout << "All done" << std::endl;
    return 0;
}