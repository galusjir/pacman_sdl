#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <exception>
#include <vector>
#include <memory>
#include <set>
#include "direction.h"
#include "pacmanAssets.h"
#include "tile.h"
#include "pacmanEngine.h"
#include "config.h"


class PacmanEngine;


/** @brief  Contains the background tiles and interacts with them
 * 
 *  The game interacts with 
 *  @param layout   path to the layout on the drive
 *  @param assets   Assests for the rendering
 */
class Maze
{
public:
  Maze(std::string layout, PacManAssets & assets, PacmanEngine * engine);
  virtual ~Maze() noexcept;

   /** @brief Check if it's possible to step on the tile
   * 
   *  @param x,y are the coordinates of the tiles
   *  @return true if it's possible to step -> movement permited
   *  @return false not possible to step on the tile. 
   */
  bool step_on(int x, int y) const ;

  /** @brief Steps on a tile. If there is a bonus, it will be collected 
   * 
   *  @param x,y are the coordinates of the tile
   *  @return 0 if it's possible to step, 
   *  @return >1 if there is bonus
   *  @return -1 if not possible to step on the tile
   */
  int step_on_bonus(int x, int y);

  /** @brief Returns current score on the maze
   * 
   */
  int get_score() const {return m_score;}
  
  /** @brief Inrements the score, when ghost is killed.
   * 
   *  Interface for PacmanEngine, which controls the dynamic object
   *  collision detection
   */
  void killed_ghost() {m_score += conf::SCORE_FOR_GHOST;}

  /** @brief Draw the maze on the screen
   * 
   *  @param engine is PacmanEngine which contains renderer
   */
  void draw(PacmanEngine * engine);
  
  /** @brief Check if all the points have been collected
   *
   *  @return false if there are points left in the maze
   *  @return true  if all points were collected
   */
  bool finish() const {return m_n_points > 0 ? false : true;}

  /** @brief Get pacman position from the layout. If not given in the layout then x, y = 0
   *   
   *  @param x,y will be modified with pacman position
   */
  void get_pacman_init_position(int & x, int & y) const
  {x = m_init_pacman_pos_x; y = m_init_pacman_pos_y;}

  /** @brief Calculate shortest distance from source to target
   * 
   *  @return Direction to the next step
   */
  Direction path_find(Position source, Position target, Direction curr_dir) const;

  /**
   * @brief get coordinates of a slot for a ghost in a penhous. There should be
   *        at least 4 ghosts
   * 
   * @param Direction Valid are NONE or SOUTH for center slot
   *                            NORTH for top out of penhouse (BLINKY)
   *                            WEST  for CLYDE
   *                            EAST for Blinky
   */
  Position get_penhouse_pos(Direction dir);

  /** @brief Get a number of tiles in the maze
   *  
   *  @param n_width   number of tiles on the width
   *  @param n_height  number of tiles on the hight
   */
  void get_n_tiles(int & n_width, int & n_height) {n_width = m_width; n_height = m_height;}
  
  /**
   * @brief Release all the tiles. Shall be called before destructing the engine (Renderer)
   */
  void clean() noexcept;

protected:

  /**
   * @brief Loads layout from a file
   */
  void load_layout(std::string layout_path, std::ifstream & in_file, PacManAssets & assets);

  /** @brief Corrects angles of a walls and corners of a maze
   * 
   *  Goes through the maze and replace wall place holder with wall and corner
   *  with proper rotation angle and proper textures
   */
  void build_walls();

  /**
   * @brief Follows a wall path interface and creates the walls
   * 
   * @param pos Start position
   * @param visited_cells Algo insertes visited tiles there
   */
  void follow_wall(Position pos, std::set<Position> & visited_cells);

  /**
   *  @brief Cross Arrow around given position with tile types aroud.
   * 
   *  Gives NORTH, WEST, EAST, SOUTH positions. No diagonal information
   */
  CrossArrow get_cross_arrow(int x_coor, int y_coor) const;
  CrossArrow get_cross_arrow(Position) const;

  /**
   * @brief Get a pointer to a tile on specified position
   * 
   * No checking of bounds!! Possible to sig
   */
  std::shared_ptr<Tile> get(int x, int y) const { return tiles_arr[x + m_width * y];}
  std::shared_ptr<Tile> get(Position pos) const { return tiles_arr[pos.get_x() + m_width * pos.get_y()];}

  

  /** @brief Replace tile on given position.
   * 
   *  @param x, y coordinates
   *  @param tile share pointer to a new tile. // Could be possible to use unique_ptr, but would need changes 
   *              to the architecture
   */
  bool replace(int x, int y, std::shared_ptr<Tile> tile ); /** < Replace tile in a maze. Deletes the old tile*/
  bool replace(Position pos, std::shared_ptr<Tile> tile ); /** < Replace tile in a maze. Deletes the old tile*/

  std::shared_ptr<Tile> * tiles_arr; /**< Stores all the tiles */
  int m_width;                       /**< width of the maze */
  int m_height;                      /**< Height of the maze */
  int m_score;                       /**< Score of a player */
  PacManAssets * m_assets;           /**< reference for the assets. Potencial danger of using deleted object! */

  int m_n_points;                    /**<< Number of points to collect */

  int m_init_pacman_pos_x;           /**< Pacman initial x position */
  int m_init_pacman_pos_y;           /**< Pacman initial x position */

  PacmanEngine * m_engine;             /**< Game engine for the drawing. Possibly not required */
};
