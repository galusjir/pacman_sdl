#include "dynamicObject.h"


DynamicObject::DynamicObject(int pos_x, int pos_y, SDL_Texture * sprt)
{
  pos_x_1000th = init_pos_x_1000th = pos_x * 1000 + 500;
  pos_y_1000th = init_pos_y_1000th = pos_y * 1000 + 500;
  m_sprt = sprt;
  current_tile = {pos_x_1000th / 1000, pos_y_1000th / 1000};
  m_kill_timer = 0;
  m_hold_timer = 0;
  m_immortal = false;
}

void DynamicObject::draw(GameEngine * engine) const
{

  SDL_Rect clipRect = {m_display * 64, 0, 64, 64};
  engine->draw_spritesheet( m_sprt, pos_x_1000th / 1000.0F, pos_y_1000th / 1000.0F,
                            m_angle, m_flip, clipRect, -32);
}

int DynamicObject::step(GameEngine::KEY_PRESS direction, Maze * maze)
{
  int x = pos_x_1000th;
  int y = pos_y_1000th;

  auto move_vector = std::make_pair(0, 0);

  if (direction == GameEngine::KEY_PRESS_UP && (x % 1000 == 500) )
  {
    y = pos_increment(y, true);
    m_flip = SDL_FLIP_NONE;
    m_angle = -90.0F;
    move_vector  = {0,-500};
  }
  else if (direction == GameEngine::KEY_PRESS_DOWN && (x % 1000 == 500) )
  {
    y = pos_increment(y, false);
    m_flip = SDL_FLIP_NONE;
    m_angle = 90.0F;
    move_vector = {0,499};
  }
  else if (direction == GameEngine::KEY_PRESS_LEFT && (y % 1000 == 500) )
  {
    x = pos_increment(x, true);
    m_flip = SDL_FLIP_HORIZONTAL;
    m_angle = 0.0F;
    move_vector = {-500,0};
  }
  else if (direction == GameEngine::KEY_PRESS_RIGHT && (y % 1000 == 500) )
  {
    x = pos_increment(x, false);
    m_flip = SDL_FLIP_NONE;
    m_angle = 0.0F;
    move_vector = {499,0};
  }
  else
  {
    return 0;
  }
  
  if(  maze->step_on((x + move_vector.first) / 1000, (y + move_vector.second) / 1000) )
  {
    current_tile = { (x + move_vector.first) / 1000, (y + move_vector.second) / 1000};
    pos_x_1000th = x;
    pos_y_1000th = y;
    return 1;
  }
  else
  {
    return 0;
  }
}


void DynamicObject::set_speed(int speed_proc)
{
  if (speed_proc < 0)
    return;
  m_speed = (conf::SPEED * speed_proc) / 100;
}
void DynamicObject::reset(int x, int y)
{
  reset();
  pos_x_1000th = 1000 * x + 500;
  pos_y_1000th = 1000 * y + 500;
}

void DynamicObject::reset()
{
  pos_x_1000th = init_pos_x_1000th;
  pos_y_1000th = init_pos_y_1000th;
  set_hold_tile();
  set_hold_timer();
  m_direction = GameEngine::KEY_PRESS_DEFAULT;
  m_angle = 0.0F;
  m_flip = SDL_FLIP_NONE;
}

int DynamicObject::pos_increment(int pos, bool negative) const
{
  int sign = negative ? (-1) : 1;
  // Position must be snapped to 500 and 1000
  int mod_to_500 = pos % 500;
  int new_pos;
  if ( mod_to_500 != 0 &&  sign < 0 && mod_to_500 < m_speed )
  {
    new_pos = pos - mod_to_500;
  }
  else if ( mod_to_500 != 0 && sign > 0 &&
           (500 - mod_to_500) < m_speed
          )
  {
    new_pos = pos + 500 - mod_to_500;
  }
  else
  {
    new_pos = pos + (sign * m_speed);
  }
  return new_pos;
}

std::pair<int, int> DynamicObject::get_position() const
{
  return std::make_pair<int, int>(pos_x_1000th / 1000, pos_y_1000th / 1000);
}

bool DynamicObject::am_i_dead()
{
  bool ret_value = false;
  if (m_kill_timer > 0)
  {
    m_kill_timer--;
    if(m_kill_timer == 0)
    {
      reset(); // Reset also set the hold timer!
    }
    ret_value = true;
  }

  if(m_hold_timer > 0)
  {
    m_hold_timer--;
    if(m_hold_timer == 0)
    {
      set_alive_tile();  // Can move again
    }
    ret_value = true;
  }

  return ret_value;
}

bool DynamicObject::kill()
{
  bool ret_value = false;
  if(!m_immortal && m_kill_timer == 0 && m_hold_timer == 0 )
  { // will be killed
    set_dead_tile();
    set_dead_timer();  // It is dead. hold the dead tile and then reset the character
    ret_value = true;

  }
  return ret_value;
}


/***************************************************/
/**           CLASS PacmanPlayer                  **/
/***************************************************/

PacmanPlayer::PacmanPlayer(int x, int y, SDL_Texture * sprt)
: DynamicObject(x, y, sprt)
{ 
  m_immortal = false;
}

/** @brief The pacman overlaps 16 pixels to each direction
 * 
 */
int PacmanPlayer::next_step(GameEngine::KEY_PRESS current_key, Maze * maze)
{
  static int display_debouncing  = 0;
  
  if( am_i_dead() )
  {
    return 0;
  }

  if (display_debouncing < 1)
  {
    m_display = CLOSED;
  }
  else
  {
    display_debouncing--;
  }

  
  if ( current_key == GameEngine::KEY_PRESS_DEFAULT)
  {
    if(m_last_pressed_key_debouncing > 0)
    {
      current_key = m_last_pressed_key;
      m_last_pressed_key_debouncing--;
    }
  }
  else
  { // User pressed some key
    m_last_pressed_key_debouncing = conf::LAST_PRESSED_KEY_DEBOUNCING;
    m_last_pressed_key = current_key;
  }
  

  int step_possible = step(current_key, maze);
  if(step_possible < 1)
  { // Try also former direction
    step_possible = step(m_direction, maze);
  }
  else
  { // Step was successfull, update the direction taraveling direction
    m_direction = current_key;
  }

  //  COLLECT THE BONUS
  if (step_possible > 0)
  { // Check if bonus is present to collect it and open a mouth
     if(maze->step_on_bonus(current_tile.first, current_tile.second) > 0)
     { 
        m_display = OPEN;
        display_debouncing = mouth_debouncing;
     }
     m_last_pressed_key = current_key;
  }

  return 0;
}


void PacmanPlayer::scatter_mode()
{
  m_immortal = true;
}


void PacmanPlayer::chase_mode()
{
  m_immortal = false;
}


void PacmanPlayer::reset()
{
  DynamicObject::reset();
  m_last_pressed_key = GameEngine::KEY_PRESS_DEFAULT;
}


/***************************************************/
/**               CLASS GHOST                     **/
/***************************************************/

//DynamicObject::DynamicObject(int pos_x, int pos_y, SDL_Texture * sprt)
Ghost::Ghost(int pos_x, int pos_y, SDL_Texture * sprt, PacmanEngine * engine) :
DynamicObject(pos_x, pos_y, sprt), m_p_engine(engine)
{ // TODO: Find the penhous and make some sensible initialization
  m_next_direction = NONE;
  m_immortal = true;
  m_direction = NONE;
}

int Ghost::next_step(GameEngine::KEY_PRESS __tmp, Maze * maze)
{
  // Add dead check
  if( am_i_dead() )
  {
    return 0;
  }
  // Crossing of a border
  if ( ( pos_x_1000th % 1000 == 0 && pos_y_1000th % 1000 == 500)  || // vertical movement
       ( pos_x_1000th % 1000 == 500 && pos_y_1000th % 1000 == 0)  || // horizontal movement
         m_next_direction == NONE
     )
  { // decide the next step, Otherwise continue with the same direction
    Position target;
    if (m_immortal)
    {
      target = get_target();
    }
    else
    {
      target = get_scater_target();
    }
    m_next_direction = maze->path_find(Position(current_tile), Position(target), m_direction);
    //std::cout << "Making new decision x: " << pos_x_1000th / 100 
    //          << "  y: " << pos_y_1000th / 1000 << std::endl;
  }

  // Change the direction in the middle of the tile
  if(pos_x_1000th % 1000 == 500 && pos_y_1000th % 1000 == 500)
  {
    m_direction = m_next_direction;
  }

  // If stepping is not for some reason possible, chose different direction
  if ( !step( dir_to_key(m_direction), maze) )
  {
    m_next_direction = NONE;
  }
  return 1;
}


void Ghost::scatter_mode()
{
  m_immortal = false;
  set_scater_tile();
  m_direction = reverse_dir(m_direction);
}

void Ghost::chase_mode()
{
  m_immortal = true;
  if (m_display != DEAD)
  {
    set_alive_tile();
  }
  m_direction = reverse_dir(m_direction);
}

void Ghost::reset()
{
  DynamicObject::reset();
  m_direction = NONE;
  m_next_direction = NONE;
}


/***************************************************/
/**      CLASS INKY, PINKY, BLINKY, AND CLYDE     **/
/***************************************************/

Position Blinky::get_target() const
{
  std::pair<Position, GameEngine::KEY_PRESS>  pos_dir = 
           m_p_engine->get_dynamic_object_position("pacman");
  
  return pos_dir.first;
}

Position Blinky::get_scater_target() const
{
  return Position(-2, 40);
}


Position Pinky::get_target() const
{
  std::pair<Position, GameEngine::KEY_PRESS>  pos_dir = 
           m_p_engine->get_dynamic_object_position("pacman");

  Position target = pos_dir.first;
  for (int i = 0; i < 4; i++)
  {
    target = target + pos_dir.second;
  }
  return target;
}

Position Pinky::get_scater_target() const
{
  return Position(-2, 2);
}

Position Inky::get_target() const
{
  std::pair<Position, GameEngine::KEY_PRESS>  pos_dir = 
           m_p_engine->get_dynamic_object_position("pacman");
  Position target = pos_dir.first + pos_dir.second; 
  target = target + pos_dir.second;

  // Get blinky position
  std::pair<Position, GameEngine::KEY_PRESS> blinky_pos_dir = 
           m_p_engine->get_dynamic_object_position("blinky");
  for (int i = 0; i < 3; i++)
  {
    target = target + blinky_pos_dir.second;
  }

  return target;
}

Position Inky::get_scater_target() const
{
  return Position (40, 40);
}

Position Clyde::get_target() const
{
  Position target;
  std::pair<Position, GameEngine::KEY_PRESS>  pos_dir = 
           m_p_engine->get_dynamic_object_position("pacman");
  if( pos_dir.first.get_distance( current_tile ) > 8)
  {
    target = pos_dir.first;
  }
  else
  {
    target = get_scater_target();
  }
  return target;
}

Position Clyde::get_scater_target() const
{
  return Position (40, 2);
}