#pragma once
#include <map>
#include <string>
#include <experimental/filesystem>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <exception>

namespace fs = std::experimental::filesystem;

/** @brief Loades and manages all assets.
 * 
 *         After initializing, first set_renderer() which will render the assets!
 */
class PacManAssets
{
public:
  static PacManAssets & get()
  {
      static PacManAssets instance;
      return instance;
  }

  PacManAssets( PacManAssets const &) = delete;
  void operator=( PacManAssets const &) = delete;

  void set_renderer(SDL_Renderer * renderer)
    { m_gRenderer = renderer;}

  SDL_Texture * get_texture(std::string & name)
  {
      return m_texture_container[name];
  }

  SDL_Texture * get_texture(const char * name)
  {
      return m_texture_container[std::string(name)];
  }

  TTF_Font * get_font(int size);

  int load_texture();

  void clean() noexcept;

private:
  PacManAssets(): m_gRenderer(nullptr) {}
  ~PacManAssets();

  void load_font(int size);

  SDL_Renderer * m_gRenderer;

  std::map<std::string, SDL_Texture *> m_texture_container;
};