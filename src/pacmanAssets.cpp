#include "pacmanAssets.h"
#include <iostream>
#include <thread>

int PacManAssets::load_texture()
{
  if (m_gRenderer == nullptr) return 1;
  
  /**< Lambda function to load all the assets */
  auto load = [&](std::string f_name, fs::path l_path )
  {
    SDL_Surface * load_surface = IMG_Load(l_path.c_str());
    if (load_surface == NULL)
    {
      throw std::invalid_argument(std::string("Sprite could not be loaded: ") + l_path.c_str() );
    }

    // What dows it do??
    SDL_SetColorKey( load_surface, SDL_TRUE, SDL_MapRGB( load_surface->format, 0, 0xFF, 0xFF ) );
   
    SDL_Texture * load_texture = SDL_CreateTextureFromSurface(m_gRenderer, load_surface);
    SDL_FreeSurface(load_surface);
    m_texture_container[f_name] = load_texture;
  };

  // ******          DYNAMIC TEXTURES        ******
  //    GHOSTS 16x16 sprite  (64x64 pixels)
  load( "inky_sprt" , fs::path ("src/graphics/blueGhost_spreadsheet_expr.png") );
  load( "clyde_sprt" , fs::path ("src/graphics/orangeGhost_spreadsheet_expr.png") );
  load( "blinky_sprt" , fs::path ("src/graphics/readGhost_spreadsheet_expr.png") );
  load( "pinky_sprt" , fs::path ("src/graphics/pinkGhost_spreadsheet_expr.png") );
  
  // PACMAN 16x16 sprite  (256x64 pixels)
  load( "pacman_sprt" , fs::path ("src/graphics/pacman_spreadsheet_expr.png") );
  
  // TODO Static textures can be drawn by the application
  // ******          STATIC TEXTURES        ******
  //  MAZE 8x8 sprite  (32x32 pixels)
  load( "empty" , fs::path ("src/graphics/empty.png") );
  load( "bonus" , fs::path ("src/graphics/bonus.png") );
  load( "coin" , fs::path ("src/graphics/coin.png") );
  load( "wall" , fs::path ("src/graphics/wall.png") );
  load( "corner" , fs::path ("src/graphics/corner_highres.png") );

  load( "heart", fs::path("src/graphics/heart_expr.png") );

  return 0;
}

PacManAssets::~PacManAssets()
{
  clean();
}

void PacManAssets::clean() noexcept
{
  for (auto & asset : m_texture_container)
  {
    SDL_DestroyTexture(asset.second);
    asset.second = NULL;                // Safer for use after release
  }
  m_texture_container.clear();
}