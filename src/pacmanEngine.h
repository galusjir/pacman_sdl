#pragma once
#include <map>

#include "gameEngine.h"
#include "maze.h"
#include "dynamicObject.h"
#include "config.h"

//**************  FORWARD DECLARATIONS  **************//
class DynamicObject;
class Maze;
//****************************************************//



class PacmanEngine : public GameEngine
{
public:
  PacmanEngine() : GameEngine() { m_app_name = "Pacman";}
  virtual ~PacmanEngine() noexcept;
  
  /**
   *  @brief Copy constructor and assignment constructor is deleted! We don't need two parallel running games
   */
  PacmanEngine(const PacmanEngine &) = delete;  // A bit paranoic, but whatever
  void operator=(const PacmanEngine &) = delete;

  /**
   * @brief Initialization and allocation of the game's resources
   * 
   *  Initializes the maze and dynamic objects
   */
  virtual bool OnUserCreate() override;
  

  /**
   * @brief Stepping forward in the game. Called cyclically byt game_thread
   */
  virtual bool OnUserUpdate(float elapsed_time) override;
  
  /**
   *  @brief Get tile position and direction of a dynamic object. 
   * 
   *  @param name of the dynamic object used when inserting to a map.
   *  @return pair of object's position and direction - GameEngine press key
   */
  std::pair<Position, GameEngine::KEY_PRESS> get_dynamic_object_position(const std::string & name) const;

  /** @brief Set's scatter mode - ghosts goes to their corners and doesn't chase pacman
   * 
   *  Maze will contain table with specified durations according to a certain number of points
   */
  void set_scatter_mode(int duration);

  /**
   *  @brief Switch dynamic objects back to chase mode
   */
  void set_chase_mode();

private:
  /**
   * @brief Initialize pacman and ghosts
   * 
   * @param assets sprite container which was reneder by m_gRenderer
   */
  void initialize_dynamics(PacManAssets & assets);

  /**
   * @brief Simple collision detection between pacman and ghosts
   *  
   *  In case of collision one of the object is killed and reseted to
   *  initial position
   */
  void check_dynamic_object_collisions();

  /**
   * @brief Check if it shall switch to chase mode
   */
  void check_switch_to_chase_mode();

  /**
   * @brief Display simple end message on a screen.
   * 
   *  No distinction between Game over and player winning
   */
  void display_end_message();
  
  /**
   * @brief Interface for the GameEngine for window resize function.
   */
  void get_screen_dimentions(int & screen_width, int & screen_height) const override;

  /**
   * @brief Draw heart and score on a screen
   */
  void draw_stats();

  /**
   * @brief Release all resources and quits SDL.
   * 
   * MUST BE CALLED UPON EXIT
   */
  virtual void quit() override;

  std::map<std::string , DynamicObject *> m_dyn_obj;
  Maze * maze = nullptr;

  int m_scatter_timer = 0;
  int n_pacman_lives = 3;

  int m_screen_width = 0;
  int m_screen_height = 0;
};