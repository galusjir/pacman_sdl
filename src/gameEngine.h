#pragma once
#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <exception>
#include <thread>
#include <string>
#include <vector>

#include "config.h"
#include "fontContainer.h"

#define UNUSED(x) (void)(x)

class GameEngine
{
public:
  /** @initialize the SDL library
   */
  GameEngine();
  virtual ~GameEngine() noexcept;
  void start();
  virtual bool OnUserCreate() = 0;
  virtual bool OnUserUpdate(float elapsed_time) = 0; // This shall be called cyclicly

  
  int draw_sprite(SDL_Texture * texture, int x, int y, double angle,
                  SDL_RendererFlip flip, int offset_x = 0, int offset_y = 0);
  
  int draw_spritesheet(SDL_Texture * texture, float x, float y, double angle,
                                 SDL_RendererFlip flip, SDL_Rect clipRect, 
                                 int offset);

  /**
   * @brief Drawing message on a screen
   * 
   * @param x_px, y_px Start position of the message in pixels
   */
  void draw_text(const std::string & message, float pos_x, float pos_y, float angle, int font_size);
  
  /**
   * @brief Draw message in a grid cell
   * 
   *  @param pos_x, pos_y is a start tile of the message
   */
  void draw_text_in_grid(const std::string & message, float pos_x, float pos_y, int font_size);

  SDL_Renderer * get_renderer() const {return m_gRenderer;}

  /**< Keyboard input. Public for better simplicity */
  enum KEY_PRESS
  {
    KEY_PRESS_DEFAULT = 0,
    KEY_PRESS_UP = 1,
    KEY_PRESS_DOWN = 2,
    KEY_PRESS_LEFT = 4,
    KEY_PRESS_RIGHT = 8
  } pressed_key;

  /**
   * @brief resize the window to a actual number of tiles
   */
  void resize_window(int x_tiles, int y_tiles);

protected: 
  /**
   * @brief Initialize window and all plugins 
   */
  bool init_window();
  
  /**
   * @brief Runs game loop
   */
  void game_thread();

  /**
   *  @brief Clears the screen and delete unnecessary text textures
   */
  void clear_screen();

  /**
   * @brief Interface for the GameEngine for window resize function.
   * 
   *  Shall be implemented in inherited class.
   */
  virtual void get_screen_dimentions(int & screen_width, int & screen_height) const = 0;

  /**
   *  @brief Deletes the window and renderer and quits sdl.
   */
  virtual void quit();

protected:  // Protected member variables
  bool m_game_active;
  std::string m_app_name; // Need to be initialized in inherited class
  int m_x_screen_offset;
  int m_y_screen_offset;
  
  SDL_Window * m_gWindow = NULL;
  SDL_Renderer * m_gRenderer = NULL;
  //SDL_Event m_event;           /**<  Handler for keyboard input output */
  
  std::vector<SDL_Texture *> m_text_pool;
  FontContainer m_font_container;
};
