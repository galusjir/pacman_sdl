#pragma once
#include <string>
#include <SDL2/SDL.h>
#include "gameEngine.h" // Access to Draw sprite method

const int BONUS_VALUE = 300;
const int COIN_VALUE  = 100;
const int SPRITE_SIZE = 32;

/** @brief Basic class for static elements. 
 *  
 *  Texture is 8x8 (32x32 pixels) wide. 
 */
class Tile
{
public:
  virtual ~Tile() {}

  virtual void draw(GameEngine * engine, int x, int y) const {}
  virtual bool is_solid() const {return m_solid;}
  
  /** @brief Returns if it's possiblo to step on the tile
   * 
   *   @param ret_value  -1 not possible to step in
   *                      0 possible to step in, no bonus
   *                    > 0 amount of bonus
   */
  virtual int step_in() {return -1;}
  
protected:
  bool m_solid = true;
};


//0***************************************************0
//                   Wall
//0***************************************************0

class Wall : public Tile 
{
public:
  Wall(SDL_Texture * texture, float angle, SDL_RendererFlip flip);

  virtual void draw(GameEngine * engine, int x, int y) const override;
  
protected:
  SDL_Texture * m_texture;
  SDL_RendererFlip m_flip;
  float m_angle;
};



//0***************************************************0
//                   Path
//0***************************************************0

class Path : public Tile
{
public:
  /** @brief Object representing empty space, coin and bonus
   * 
   *  @param t1 shall be default texture after stepping in - empty
   *  @param t2 Initial texture. If null, then empty space
   */
  Path(SDL_Texture * t1, SDL_Texture * t2, bool is_bonus);

  virtual void draw(GameEngine * engine, int x, int y) const override;
  
  virtual int  step_in() override;

protected:
  SDL_Texture * textures[2];
  int m_draw_texture;
  bool m_is_bonus;
  int  m_score;
};