#include "direction.h"

GameEngine::KEY_PRESS dir_to_key(Direction dir)
{
  GameEngine::KEY_PRESS key;

  switch (dir)
  {
    case NONE:  key = GameEngine::KEY_PRESS_DEFAULT; break;
    case NORTH: key = GameEngine::KEY_PRESS_UP; break;
    case SOUTH: key = GameEngine::KEY_PRESS_DOWN; break;
    case EAST:  key = GameEngine::KEY_PRESS_LEFT; break;
    case WEST:  key = GameEngine::KEY_PRESS_RIGHT; break;
  }
  return key;
}

Direction key_to_dir(GameEngine::KEY_PRESS key)
{
  Direction dir;
  switch (key)
  {
    case GameEngine::KEY_PRESS_DEFAULT: dir = NONE; break;
    case GameEngine::KEY_PRESS_UP: dir = NORTH; break;
    case GameEngine::KEY_PRESS_DOWN: dir = SOUTH; break;
    case GameEngine::KEY_PRESS_LEFT: dir = EAST; break;
    case GameEngine::KEY_PRESS_RIGHT: dir = WEST; break;
  }
  return dir;
}

Direction turn_right(Direction dir)
{
  Direction new_dir;
  switch (dir)
  {
    case NONE: new_dir = NONE; break;
    case NORTH: new_dir = WEST; break;
    case WEST: new_dir = SOUTH; break;
    case SOUTH: new_dir = EAST; break;
    case EAST: new_dir = NORTH; break;
  }
  return new_dir;
}

Direction turn_left(Direction dir)
{
  Direction new_dir;
  switch (dir)
  {
    case NONE: new_dir = NONE; break;
    case NORTH: new_dir = EAST; break;
    case EAST: new_dir = SOUTH; break;
    case SOUTH: new_dir = WEST; break;
    case WEST: new_dir = NORTH; break;
  }
  return new_dir;
}

Direction reverse_dir(Direction dir)
{
  Direction reverse_dir = NONE;
  switch (dir)
  {
    case NONE:  reverse_dir = NONE; break;
    case NORTH: reverse_dir = SOUTH; break;
    case SOUTH: reverse_dir = NORTH; break;
    case EAST:  reverse_dir = WEST; break;
    case WEST:  reverse_dir = EAST; break;
  }
  return reverse_dir;
}