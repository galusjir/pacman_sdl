#Zde bude funkcni Makefile.
LD=g++
CXX=g++
CXXFLAGS = -Wall --pedantic -std=c++17
LDFLAGS = -lSDL2 -lSDL2_image -lSDL2_ttf -lpthread -lstdc++fs
BUILD_DIR = ./build
SOURCE_DIR = ./src
PROG_NAME  = pacman 

SRC_LIST=$(wildcard $(SOURCE_DIR)/*.cpp)
OBJ_LIST= $(patsubst %, $(BUILD_DIR)/%, $(notdir $(SRC_LIST:.cpp=.o)) )

#SRC_LIST=$(SOURCE_DIR)/*.cpp
#OBJ_LIST=$(SRC_LIST:%.cpp=$(BUILD_DIR)/%.o)


debug: CXXFLAGS += -DDEBUG -g

debug: $(PROG_NAME)

all: deps $(PROG_NAME) 

compile: $(PROG_NAME)

.PHONY print:
	$(info   SOURCE LIST: $(SRC_LIST))
	$(info   OBJECT LIST: $(OBJ_LIST))

$(PROG_NAME): $(OBJ_LIST)
	$(LD) -o $@ $^ $(LDFLAGS)

clean:
	rm -f $(BUILD_DIR)/$(PROG_NAME) $(BUILD_DIR)/*.o $(PROG_NAME) $(BUILD_DIR)/Makefile.d
	rm -r html latex

run:
	./$(PROG_NAME)
# One possibility is to do it like this:
$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

deps:
	rm -f $(BUILD_DIR)/Makefile.d
	$(CXX) -MM $(SOURCE_DIR)/*.cpp >> $(BUILD_DIR)/Makefile.d

-include Makefile.d


doc:
	@doxygen ./doxygen_conf.txt

# $@ prvni predpoklad
# $< druhy parametr
# $^  vsechny parametry

